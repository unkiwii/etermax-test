package org.unkiwii.etermax;

import java.util.HashSet;
import java.util.Set;

import org.unkiwii.etermax.actions.Action;
import org.unkiwii.etermax.actions.Damage;
import org.unkiwii.etermax.actions.Heal;

public class Character {
	private static final double MAX_HEALTH = 1000;
	
	private static final double INIT_HEALTH = MAX_HEALTH;
	private static final int INIT_LEVEL = 1;
	private static final boolean INIT_ALIVE = true;

	private String name;
	
	private double health;
	private int level;
	private boolean alive;
	
	private Damage damageAction;
	private Heal healAction;

	private Position position;

	private Set<String> factions;
	
	public Character(String name, Damage damage, Heal heal) {
		this.name = name;
		this.health = INIT_HEALTH;
		this.level = INIT_LEVEL;
		this.alive = INIT_ALIVE;
		this.damageAction = damage;
		this.healAction = heal;
		this.position = new Position(0, 0);
		this.factions = new HashSet<String>();
	}
	
	public void move(Position to) {
		this.position = to.clone();
	}
	
	public String getName() {
		return this.name;
	}

	public double getHealth() {
		return this.health;
	}

	public int getLevel() {
		return this.level;
	}
	
	public void levelUp() {
		this.level++;
	}

	public boolean isAlive() {
		return this.alive;
	}
	
	public Position getPosition() {
		return this.position.clone();
	}
	
	public boolean damage(Character other) {
		return this.damageAction.apply(this, other);
	}
	
	public boolean heal(Character other) {
		return this.healAction.apply(this, other);
	}

	public double distanceTo(Character other) {
		return this.position.distanceTo(other.position);
	}
	
	public void receiveDamage(Action.Tag tag, double power) {
		tag.hashCode();	// verify tag
		this.health -= power;
		if (this.health <= 0) {
			this.health = 0;
			this.alive = false;
		}
	}

	public void receiveHealing(Action.Tag tag, double power) {
		tag.hashCode();	// verify tag
		this.health += power;
		if (this.health >= MAX_HEALTH) {
			this.health = MAX_HEALTH;
		}
	}
	
	public void join(String faction) {
		this.factions.add(faction);
	}
	
	public void leave(String faction) {
		this.factions.remove(faction);
	}
	
	public Set<String> getFactions() {
		return new HashSet<String>(this.factions);
	}
	
	public boolean isEnemyOf(Character other) {
		for (String faction : other.factions) {
			if (this.factions.contains(faction)) {
				return false;
			}
		}
		
		return true;
	}
}
