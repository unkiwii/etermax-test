package org.unkiwii.etermax;

public class Position {
	private double x;
	private double y;

	public Position(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public Position clone() {
		return new Position(this.x, this.y);
	}
	
	private static final double EPSILON = 1e-10;
	
	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof Position)) {
			return false;
		}
		Position o = (Position)other;
		return this.x - o.x <= EPSILON &&
				this.y - o.y <= EPSILON;
	}
	
	public double distanceTo(Position other) {
		double xd = this.x - other.x;
		double yd = this.y - other.y;
		return Math.sqrt(xd * xd + yd * yd);
	}
}
