package org.unkiwii.etermax.actions;

import org.unkiwii.etermax.Character;

public class Damage extends Action {
	
	public static final double LOW_LEVEL_MODIFIER = 1.5;
	public static final double HIGH_LEVEL_MODIFIER = 0.5;
	public static final int LEVEL_MODIFIER_THRESHOLD = 5;	// levels
	
	private double power;
	
	public Damage(double power) {
		this.power = power;
	}
	
	public double getPower() {
		return power;
	}

	@Override
	public boolean apply(Character actor, Character target) {
		if (target == actor) {
			// an actor can't damage itself
			return false;
		}
		
		if (!target.isEnemyOf(actor)) {
			// an actor can't damage an ally
			return false;
		}
		
		double power = this.power;
		
		if (actor.getLevel() + LEVEL_MODIFIER_THRESHOLD <= target.getLevel()) {
			power *= HIGH_LEVEL_MODIFIER;
		}
		
		if (target.getLevel() + LEVEL_MODIFIER_THRESHOLD <= actor.getLevel()) {
			power *= LOW_LEVEL_MODIFIER;
		}
		
		target.receiveDamage(Action.tag, power);
		
		return true;
	}

}
