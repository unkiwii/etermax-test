package org.unkiwii.etermax.actions;

import org.unkiwii.etermax.Character;

public class Heal extends Action {
	
	private double power;
	
	public Heal(double power) {
		this.power = power;
	}
	
	public double getPower() {
		return power;
	}

	@Override
	public boolean apply(Character actor, Character target) {
		if (!target.isAlive()) {
			// can't heal the dead
			return false;
		}
		
		if (target.isEnemyOf(actor)) {
			// can't heal enemies
			return false;
		}
		
		target.receiveHealing(Action.tag, this.power);
		
		return true;
	}

}
