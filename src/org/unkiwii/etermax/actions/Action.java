package org.unkiwii.etermax.actions;

import org.unkiwii.etermax.Character;

public abstract class Action {
	/**
	 * Tag class is used to simulate a 'friend class' (as in C++)
	 * Only objects of type Rule can actuate on RPGActors so this
	 * tag allow us to update an RPGActor state.
	 */
	public static final class Tag {
		private Tag() {}
	}
	protected static final Tag tag = new Tag();
	
	/**
	 * Applies the rule using an actor to a given target.
	 * 
	 * @param actor		The actor which is applying the rule.
	 * @param target	The target of the action. 
	 * @return <code>true</code> if the rule was applied, <code>false</code> otherwise.
	 */
	abstract public boolean apply(Character actor, Character target);
}
