package org.unkiwii.etermax.actions;

import org.unkiwii.etermax.Character;

public class MeleeDamage extends Damage {
	public static final int RANGE = 2;

	public MeleeDamage(double power) {
		super(power);	// fuck yeah!
	}

	@Override
	public boolean apply(Character actor, Character target) {
		if (actor.distanceTo(target) <= RANGE) {
			return super.apply(actor, target);
		}
		
		return false;
	}
}
