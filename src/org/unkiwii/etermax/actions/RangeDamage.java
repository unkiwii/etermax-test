package org.unkiwii.etermax.actions;

import org.unkiwii.etermax.Character;

public class RangeDamage extends Damage {
	public static final int MIN_RANGE = 5;
	public static final int MAX_RANGE = 20;
	
	public RangeDamage(double power) {
		super(power);
	}

	@Override
	public boolean apply(Character actor, Character target) {
		double distance = actor.distanceTo(target);
		
		if (distance <= MAX_RANGE && distance >= MIN_RANGE) {
			return super.apply(actor, target);
		}
		
		return false;
	}
}
