package test.org.unkiwii.etermax;

import org.unkiwii.etermax.actions.Damage;
import org.unkiwii.etermax.actions.Heal;
import org.unkiwii.etermax.Character;

import junit.framework.TestCase;

public class HealTest extends TestCase {
	
	private static final double EPSILON = 1e-6;

	public void testApply() {
		Character healer = new Character(null, null, null);
		Character damager = new Character(null, new Damage(1), null);
		Character c = new Character(null, null, null);
		
		healer.join("a");
		c.join("a");
		
		double health = c.getHealth();
		damager.damage(c);
		assertTrue(c.getHealth() < health);
		
		Heal h = new Heal(1);
		
		health = c.getHealth();
		h.apply(healer, c);
		assertTrue(c.getHealth() > health);
	}

	public void testHeal() {
		double power = Math.random() * Double.MAX_VALUE;
		Heal h = new Heal(power);
		assertNotNull(h);
		assertEquals(power, h.getPower(), EPSILON);
	}

}
