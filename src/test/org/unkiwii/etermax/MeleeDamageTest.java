package test.org.unkiwii.etermax;

import org.unkiwii.etermax.Character;
import org.unkiwii.etermax.Position;
import org.unkiwii.etermax.actions.MeleeDamage;

import junit.framework.TestCase;

public class MeleeDamageTest extends TestCase {
	
	private static final double EPSILON = 1e-6;

	public void testApply() {
		Character damager = new Character(null, null, null);
		Character near = new Character(null, null, null);
		Character far = new Character(null, null, null);
		
		damager.move(new Position(5, 5));
		near.move(new Position(5.5, 5.2));
		far.move(new Position(38, 12));
		
		assertTrue(damager.distanceTo(near) <= MeleeDamage.RANGE);
		assertTrue(damager.distanceTo(far) > MeleeDamage.RANGE);
		
		MeleeDamage md = new MeleeDamage(1);
		
		double nearHealth = near.getHealth();
		md.apply(damager, near);
		assertTrue(near.getHealth() < nearHealth);
		
		double farHealth = far.getHealth();
		md.apply(damager, far);
		assertEquals(farHealth, far.getHealth(), EPSILON);
	}

	public void testMeleeDamage() {
		double power = Math.random() * Double.MAX_VALUE;
		MeleeDamage md = new MeleeDamage(power);
		assertNotNull(md);
		assertEquals(power, md.getPower(), EPSILON);
	}

}
