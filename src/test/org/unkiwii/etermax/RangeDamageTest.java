package test.org.unkiwii.etermax;

import org.unkiwii.etermax.Character;
import org.unkiwii.etermax.Position;
import org.unkiwii.etermax.actions.RangeDamage;

import junit.framework.TestCase;

public class RangeDamageTest extends TestCase {

	private static final double EPSILON = 1e-6;
	
	public void testApply() {
		Character damager = new Character(null, null, null);
		Character near = new Character(null, null, null);
		Character good = new Character(null, null, null);
		Character far = new Character(null, null, null);
		
		damager.move(new Position(5, 5));
		near.move(new Position(5.5, 5.2));
		good.move(new Position(12, 12));
		far.move(new Position(120, 380));

		RangeDamage rd = new RangeDamage(1);
		
		double nearDistance = damager.distanceTo(near);
		assertTrue(nearDistance <= RangeDamage.MIN_RANGE);
		
		double nearHealth = near.getHealth();
		rd.apply(damager, near);
		assertEquals(nearHealth, near.getHealth(), EPSILON);
		
		double goodDistance = damager.distanceTo(good);
		assertTrue(goodDistance >= RangeDamage.MIN_RANGE && goodDistance <= RangeDamage.MAX_RANGE);
		
		double goodHealth = good.getHealth();
		rd.apply(damager, good);
		assertTrue(good.getHealth() < goodHealth);
		
		double farDistance = damager.distanceTo(far);
		assertTrue(farDistance >= RangeDamage.MAX_RANGE);
		
		double farHealth = far.getHealth();
		rd.apply(damager, far);
		assertEquals(farHealth, far.getHealth(), EPSILON);
	}

	public void testRangeDamage() {
		double power = Math.random() * Double.MAX_VALUE;
		RangeDamage rd = new RangeDamage(power);
		assertNotNull(rd);
		assertEquals(power, rd.getPower(), EPSILON);
	}

}
