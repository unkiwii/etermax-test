package test.org.unkiwii.etermax;

import org.unkiwii.etermax.Position;

import junit.framework.TestCase;

public class PositionTest extends TestCase {

	private static final double EPSILON = 1e-6;
	
	public void testPosition() {
		double x = Math.random() * Double.MAX_VALUE;
		double y = Math.random() * Double.MAX_VALUE;
		Position p = new Position(x, y);
		assertNotNull(p);
		assertEquals(p.getX(), x, EPSILON);
		assertEquals(p.getY(), y, EPSILON);
	}

	public void testClone() {
		double x = Math.random() * Double.MAX_VALUE;
		double y = Math.random() * Double.MAX_VALUE;
		Position p = new Position(x, y);
		Position b = p.clone();
		assertFalse(p == b);
		assertEquals(p.getX(), b.getX(), EPSILON);
		assertEquals(p.getY(), b.getY(), EPSILON);
	}

	public void testDistanceTo() {
		Position p = new Position(0, 0);
		Position a = new Position(3, 4);
		assertEquals(p.distanceTo(a), 5.0, EPSILON);
		assertEquals(a.distanceTo(p), 5.0, EPSILON);
		assertEquals(a.distanceTo(p), p.distanceTo(a), EPSILON);
		
		double distance = Math.sqrt(2);
		Position b = new Position(1, 1);
		assertEquals(p.distanceTo(b), distance, EPSILON);
		assertEquals(b.distanceTo(p), distance, EPSILON);
		assertEquals(b.distanceTo(p), p.distanceTo(b), EPSILON);
		
		Position c = new Position(Math.random() * Double.MAX_VALUE, Math.random() * Double.MAX_VALUE);
		Position d = new Position(Math.random() * Double.MAX_VALUE, Math.random() * Double.MAX_VALUE);
		assertEquals(c.distanceTo(d), d.distanceTo(c), EPSILON);
	}

}
