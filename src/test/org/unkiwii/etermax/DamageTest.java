package test.org.unkiwii.etermax;

import org.unkiwii.etermax.Character;
import org.unkiwii.etermax.actions.Damage;
import junit.framework.TestCase;

public class DamageTest extends TestCase {

	private static final double EPSILON = 1e-6;
	
	public void testApply() {
		Character damager = new Character(null, null, null);
		Character c = new Character(null, null, null);
		
		Damage d = new Damage(1);
		
		double health = c.getHealth();
		d.apply(damager, c);
		assertTrue(c.getHealth() < health);
	}
	
	public void testLevelModifiers() {
		Character damager = new Character(null, null, null);
		Character lowerLevel = new Character(null, null, null);
		Character higherLevel = new Character(null, null, null);
		
		int damagerLevel = 20;
		int threshold = Damage.LEVEL_MODIFIER_THRESHOLD + 2;
		for (int i = 0; i < damagerLevel - threshold; i++)	{ lowerLevel.levelUp();		}
		for (int i = 0; i < damagerLevel; i++)				{ damager.levelUp();		}
		for (int i = 0; i < damagerLevel + threshold; i++)	{ higherLevel.levelUp();	}
		
		Damage d = new Damage(10);
		
		double lowerLevelHealth = lowerLevel.getHealth();
		d.apply(damager, lowerLevel);
		assertTrue(lowerLevel.getHealth() < lowerLevelHealth);
		assertEquals(lowerLevelHealth - (d.getPower() * Damage.LOW_LEVEL_MODIFIER), lowerLevel.getHealth(), EPSILON);
		
		double higherLevelHealth = higherLevel.getHealth();
		d.apply(damager, higherLevel);
		assertTrue(higherLevel.getHealth() < higherLevelHealth);
		assertEquals(higherLevelHealth - (d.getPower() * Damage.HIGH_LEVEL_MODIFIER), higherLevel.getHealth(), EPSILON);	
	}

	public void testDamage() {
		double power = Math.random() * Double.MAX_VALUE;
		Damage d = new Damage(power);
		assertNotNull(d);
		assertEquals(power, d.getPower(), EPSILON);
	}

}
