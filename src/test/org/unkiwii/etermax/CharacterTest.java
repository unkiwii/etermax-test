package test.org.unkiwii.etermax;

import java.util.HashSet;
import java.util.Set;

import org.unkiwii.etermax.Character;
import org.unkiwii.etermax.Position;
import org.unkiwii.etermax.actions.Damage;
import org.unkiwii.etermax.actions.Heal;

import junit.framework.TestCase;

public class CharacterTest extends TestCase {
	
	private static final double EPSILON = 1e-6;

	public void testCharacter() {
		String name = "A random name";
		Character c = new Character(name, null, null);
		assertNotNull(c);
		assertEquals(name, c.getName());
		assertEquals(1, c.getLevel());
		assertTrue(c.isAlive());
	}

	public void testMove() {
		Character c = new Character(null, null, null);
		
		Position first = new Position(Math.random() * Double.MAX_VALUE, Math.random() * Double.MAX_VALUE);
		c.move(first);
		assertEquals(first, c.getPosition());
		
		Position second = new Position(Math.random() * Double.MAX_VALUE, Math.random() * Double.MAX_VALUE);
		c.move(second);
		assertEquals(second, c.getPosition());
	}

	public void testGetName() {
		String name = "some name";
		Character c = new Character(name, null, null);
		assertEquals(name, c.getName());
	}

	public void testGetLevel() {
		Character c = new Character(null, null, null);
		assertEquals(1, c.getLevel());
	}

	public void testIsAlive() {
		Character c;
		
		c = new Character(null, null, null);
		assertTrue(c.isAlive());
		
		Character otk = new Character(null, new Damage(Double.MAX_VALUE), null);
		otk.damage(c);
		assertFalse(c.isAlive());
		
		c = new Character(null, null, null);
		assertTrue(c.isAlive());
		
		Character weak = new Character(null, new Damage(1e-5), null);
		weak.damage(c);
		assertTrue(c.isAlive());
	}

	public void testDamage() {
		Character c = new Character(null, null, null);
		Character damager = new Character(null, new Damage(1), null);
		
		double health = c.getHealth();
		damager.damage(c);
		assertTrue(c.getHealth() < health);
	}
	
	public void testCantDamageMyself() {
		Character damager = new Character(null, new Damage(5), null);
		
		double health = damager.getHealth();
		damager.damage(damager);
		assertEquals(health, damager.getHealth(), EPSILON);
	}
	
	public void testCantDamageAnAlly() {
		Character damager = new Character(null, new Damage(5), null);
		Character ally = new Character(null, null, null);
		
		ally.join("a");
		damager.join("a");
		
		double health = ally.getHealth();
		damager.damage(ally);
		assertEquals(health, ally.getHealth(), EPSILON);
	}

	public void testHeal() {
		Character c = new Character(null, null, null);
		Character damager = new Character(null, new Damage(5), null);
		Character healer = new Character(null, null, new Heal(1));
		
		c.join("a");
		healer.join("a");
		
		double hiHealth = c.getHealth();
		damager.damage(c);
		assertTrue(c.getHealth() < hiHealth);
		
		double lowHealth = c.getHealth();
		healer.heal(c);
		assertTrue(c.getHealth() > lowHealth);
	}
	
	public void testCantHealAnEnemy() {
		Character damager = new Character(null, new Damage(5), null);
		Character enemy = new Character(null, null, null);
		Character healer = new Character(null, null, new Heal(1));
		
		enemy.join("a");
		healer.join("b");
				
		double health = enemy.getHealth();
		damager.damage(enemy);
		assertTrue(enemy.getHealth() < health);
		
		health = enemy.getHealth();
		healer.heal(enemy);
		assertEquals(health, enemy.getHealth(), EPSILON);
	}
	
	public void testCantHealTheDead() {
		Character dead = new Character(null, null, null);
		Character otk = new Character(null, new Damage(Double.MAX_VALUE), null);
		Character healer = new Character(null, null, new Heal(1));
		
		dead.join("a");
		healer.join("a");
		
		assertTrue(dead.isAlive());
		double health = dead.getHealth();
		assertTrue(health > 0);
		
		otk.damage(dead);
		assertFalse(dead.isAlive());
		assertEquals(0, dead.getHealth(), EPSILON);
		
		health = dead.getHealth();
		healer.heal(dead);
		assertEquals(health, dead.getHealth(), EPSILON);
	}

	public void testDistanceTo() {
		Character a = new Character(null, null, null);
		Position p = new Position(Math.random() * Double.MAX_VALUE, Math.random() * Double.MAX_VALUE);
		a.move(p);
		
		Character b = new Character(null, null, null);
		Position q = new Position(Math.random() * Double.MAX_VALUE, Math.random() * Double.MAX_VALUE);
		b.move(q);
		
		assertEquals(a.distanceTo(b), p.distanceTo(q), EPSILON);
		assertEquals(b.distanceTo(a), p.distanceTo(q), EPSILON);
		assertEquals(a.distanceTo(b), q.distanceTo(p), EPSILON);
		assertEquals(b.distanceTo(a), q.distanceTo(p), EPSILON);
	}

	public void testJoin() {
		String a = "faction a";
		String b = "faction b";
		String c = "faction c";
		
		Set<String> all = new HashSet<String>();
		all.add(a);
		all.add(b);
		all.add(c);
		
		Set<String> justA = new HashSet<String>();
		justA.add(a);
		
		Set<String> aAndB = new HashSet<String>();
		aAndB.add(a);
		aAndB.add(b);
		
		Character ch = new Character(null, null, null);
		assertTrue(ch.getFactions().isEmpty());
		
		ch.join(a);
		assertEquals(justA, ch.getFactions());
		
		ch.join(b);
		assertEquals(aAndB, ch.getFactions());
		
		ch.join(c);
		assertEquals(all, ch.getFactions());
	}

	public void testLeave() {
		String a = "faction a";
		String b = "faction b";
		String c = "faction c";
		
		Set<String> all = new HashSet<String>();
		all.add(a);
		all.add(b);
		all.add(c);
		
		Set<String> justA = new HashSet<String>();
		justA.add(a);
		
		Set<String> aAndB = new HashSet<String>();
		aAndB.add(a);
		aAndB.add(b);
		
		Character ch = new Character(null, null, null);
		assertTrue(ch.getFactions().isEmpty());
		
		ch.join(a);
		ch.join(c);
		ch.join(b);
		assertEquals(all, ch.getFactions());
		
		ch.leave(c);
		assertEquals(aAndB, ch.getFactions());
		
		ch.leave(b);
		assertEquals(justA, ch.getFactions());
		
		ch.leave(a);
		assertTrue(ch.getFactions().isEmpty());
	}

	public void testIsEnemyOf() {
		Character a = new Character(null, null, null);
		Character b = new Character(null, null, null);
		
		String horde = "horde";
		String alliance = "alliance";
		
		assertTrue(a.isEnemyOf(b));
		a.join(horde);
		assertTrue(a.isEnemyOf(b));
		b.join(horde);
		assertFalse(a.isEnemyOf(b));
		b.leave(horde);
		b.join(alliance);
		assertTrue(a.isEnemyOf(b));
	}

}
