import org.unkiwii.etermax.Position;
import org.unkiwii.etermax.actions.Heal;
import org.unkiwii.etermax.actions.MeleeDamage;
import org.unkiwii.etermax.actions.RangeDamage;
import org.unkiwii.etermax.Character;

public class Main {

	public static void main(String[] args) {
		Character warlock = new Character("Warlock", new RangeDamage(10), new Heal(2));
		warlock.join("Horde");
		warlock.join("MyGuild");
		warlock.move(new Position(5, 5));
		
		Character priest = new Character("Priest", new RangeDamage(1), new Heal(10));
		priest.join("Horde");

		Character warrior = new Character("Warrior", new MeleeDamage(8), new Heal(0));
		warrior.join("Alliance");
		
		Character paladin = new Character("Paladin", new MeleeDamage(3), new Heal(7));
		paladin.join("Alliance");
		paladin.join("BFF");
		
		System.out.println("can warlock damage paladin? " + warlock.damage(paladin));
		System.out.println("can the paladin heal himself? " + paladin.heal(paladin));
		
		System.out.println("warrior " + warrior.isAlive());
		warrior.receiveDamage(null, 1000000);
		System.out.println("warrior " + warrior.isAlive());
	}

}
